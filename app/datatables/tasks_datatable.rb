class TasksDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper

  def initialize(view,tasks)
    @view = view
    @tasks = tasks
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Task.count,
      iTotalDisplayRecords: @tasks.size,
      aaData: data
    }
  end

  private

  def data
    tasks.map do |task|
      delete_task = "¿Are you sure want to delete this task #{task.name}?"
      [
        (task.name.present? ? task.name :  ''),
        (task.project.present? && task.project.name.present? ? task.project.name : '' ),
        (task.priority.present? ? task.priority : '' ),
        (task.status.present? ? task.status : '' ),
        (task.deadline_days.present? ? task.deadline_days : 'No' ),
        (task.end_date.present? ? task.end_date.strftime("%d-%m-%Y") : '' ),
        link_to('Delete', task_path(task), method: :delete, data: { confirm: delete_task }, class: 'btn btn-xs btn-danger')+" "+
        link_to('Show', task_path(task), class: 'btn btn-xs btn-success')+" "+
        link_to('Edit', edit_task_path(task), class: 'btn btn-xs btn-warning')
      ]
    end
  end

  def tasks
    @tasks = fetch_tasks
  end

  def fetch_tasks
    tasks = @tasks.page(page).per_page(per_page)   
    if params[:sSearch].present?
      params[:sSearch] = params[:sSearch].downcase
      tasks = tasks.where("CAST(TO_CHAR(tasks.end_date, 'dd-mm-YYYY') AS TEXT) LIKE :search OR LOWER(tasks.name) LIKE :search", search: "%#{params[:sSearch]}%")
    end
    tasks
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = [ 'tasks.name', 'tasks.kind', 'tasks.start', 'tasks.end','','']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end
end
