class ProjectsDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper

  def initialize(view,data)
    @view = view
    @projects = data
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: Project.count,
      iTotalDisplayRecords: @projects.size,
      aaData: data
    }
  end

  private

  def data
    projects.map do |project|
      delete_project = "¿Are you sure want to delete this project #{project.name}?"
      [
        (project.name.present? ? project.name :  ''),
        (project.kind.present? ? project.kind : '' ),
        (project.start.present? ? project.start.strftime("%d-%m-%Y") : '' ),
        (project.end.present? ? project.end.strftime("%d-%m-%Y") : '' ),
        (project.end.present? && project.end < Time.now ? 'Yes': 'No' ),
        link_to('Delete', project_path(project), method: :delete, data: { confirm: delete_project }, class: 'btn btn-xs btn-danger')+" "+
        link_to('Show', project_path(project), class: 'btn btn-xs btn-success')+" "+
        link_to('Edit', edit_project_path(project), class: 'btn btn-xs btn-warning')
      ]
    end
  end

  def projects
    @projects = fetch_projects
  end

  def fetch_projects
    projects = @projects.order("#{sort_column} #{sort_direction}")
    projects = projects.page(page).per_page(per_page)   
    if params[:sSearch].present?
      params[:sSearch] = params[:sSearch].downcase
      projects = Project.where("CAST(TO_CHAR(projects.start, 'dd-mm-YYYY') AS TEXT) LIKE :search OR LOWER(projects.name) LIKE :search OR CAST(TO_CHAR(projects.end, 'dd-mm-YYYY') AS TEXT) LIKE :search", search: "%#{params[:sSearch]}%")
    end
    projects
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = [ 'projects.name', 'projects.kind', 'projects.start', 'projects.end','','']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'asc' : 'desc'
  end
end
