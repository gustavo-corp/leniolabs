class Project < ApplicationRecord
  has_many :tasks, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  belongs_to :user

  validates :name, :kind, :start, presence: true
  validates_uniqueness_of :name

  enum kind: [:normal, :work, :personal, :family]

  before_create :check_dates

  def check_dates
    projects = Project.where(kind: kind).pluck(:start,:end)
    i = 0
    if start != nil && self.end != nil
      while(i < projects.size)
        if projects[i][0] != nil && projects[i][1] != nil
          (projects[i][0].to_date ... projects[i][1].to_date).include?(self.start.to_date) ? throw(:abort) : ''
          (projects[i][0].to_date ... projects[i][1].to_date).include?(self.end.to_date) ? throw(:abort) : ''
        end
        i += 1
      end
    end
  end

end
