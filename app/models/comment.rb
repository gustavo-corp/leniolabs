class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true

  acts_as_paranoid
  
  validates :body, :commentable_id, :commentable_type ,presence: true

  mount_uploader :document, DocumentUploader
  
  #include Elasticsearch::Model
  #include Elasticsearch::Model::Callbacks
#
  #settings do
  #  mappings dynamic: false do
  #    indexes :body, type: :text, analyzer: :english
  #    indexes :user_id, type: :integer, analyzer: :english
  #  end
  #end
#
  #def self.search_body_texts(query)
  #  self.search({
  #    query: {
  #      bool: {
  #        must: [
  #        {
  #          multi_match: {
  #            query: query,
  #            fields: [:body]
  #          }
  #        },
  #        {
  #          match: {
  #            deleted_at: nil
  #          }
  #        }
  #        ]
  #      }
  #    }
  #  })
  #end
end
