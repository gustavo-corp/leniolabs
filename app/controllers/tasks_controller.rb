class TasksController < ApplicationController
  before_action :set_task, only: [:edit, :update,:show, :destroy]
  def index
    current_user.has_role?(:admin) ? @tasks = Task.all : @tasks = current_user.tasks
    respond_to do |format|
      format.html
      format.json { render json: TasksDatatable.new(view_context, @tasks) }
    end
  end

  def show
  end

  def edit
  end

  def create
    @task = Task.new(task_params)
    @task.project_id = params[:project_id] if params.has_key?(:project_id) && !params[:project_id].nil?
    @task.status = Task.statuses.key(params[:status].to_i) if params.has_key?(:status) && !params[:status].nil?
    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_path }
      else
        format.html { render :new }
      end
    end
  end

  def new
    @task = Task.new
  end

  def update
    respond_to do |format|
      if @task.update(task_params)  && @task.update_column(:status, params[:status])
        format.html { redirect_to tasks_path }
      else
        format.html { render :edit }
      end
    end
  end


  def destroy
    @task = @task.destroy 
    respond_to do |format|
      if @task.errors.present?
        puts ap @task.errors.messages
        format.html { redirect_to tasks_url }
        format.json { head :no_content }
      else
        format.html { redirect_to tasks_url}
        format.json { head :no_content }
      end
    end
  end

  private
  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task)
          .permit(:name, :project_id, :priority, :status, :end_date, :deadline_days)
  end


end
