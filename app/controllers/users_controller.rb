class UsersController < ApplicationController
  before_action :set_user, only: [:destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context, User.all) }
    end
  end

  def destroy
    @user = @user.destroy 
    respond_to do |format|
      if @user.errors.present?
        puts ap @user.errors.messages
        format.html { redirect_to users_url }
        format.json { head :no_content }
      else
        format.html { redirect_to users_url}
        format.json { head :no_content }
      end
    end
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

end
