class Api::ApiController < ApplicationController
  skip_authorization_check
  skip_before_action :verify_authenticity_token
  
end