module Api
  module V1
    class TasksController < V1Controller

      before_action :set_task, only: [:show]

      def show
        render json: { status: :ok, task: @task}
      end

      def index
        render json: { status: :ok, tasks: Task.all}
      end

      private
      def set_task
        @task = Task.find(params[:id])
      end

    end
  end
end
