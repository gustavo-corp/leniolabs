module Api
  module V1
    class ProjectsController < V1Controller
      before_action :set_project, only: [:show]

      def index
        render json: { status: :ok, projects: Project.all}
      end
      
      def show
        render json: { status: :ok, project: @project}
      end

      private
      #def project_params
      #  params.require(:project)
      #        .permit(:name, :start, :user_id)
      #end

      def set_project
        @project = Project.find(params[:id])
      end

    end
  end
end


