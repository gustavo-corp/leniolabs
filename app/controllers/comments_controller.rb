class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_comment, only: [:destroy]

  def index
    # only an admin user can see all comments
    current_user.has_role?(:admin) ? @comments = Comment.with_deleted : @comments = current_user.comments
    respond_to do |format|
      format.html
      format.json { render json: CommentsDatatable.new(view_context, @comments) }
    end
  end

  def destroy
    @comment = @comment.destroy 
    respond_to do |format|
      if @comment.errors.present?
        puts ap @comment.errors.messages
        format.html { redirect_to comments_url }
        format.json { head :no_content }
      else
        format.html { redirect_to comments_url}
        format.json { head :no_content }
      end
    end
  end

  def create
    @comment = @commentable.comments.new comment_params
    @comment.user = current_user
    if comment_params.has_key?(:document) && !comment_params[:document].blank?
      @comment.document = comment_params[:document]
    end
    @comment.save
    redirect_to @commentable, notice: "Your comment was successfully posted."
  end

  private
  def set_comment
    @comment = Comment.find(params[:id])
  end
  def comment_params
    params.require(:comment).permit(:body, :document)
  end
  
end