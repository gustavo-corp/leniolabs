require 'rails_helper'

RSpec.describe Task, type: :model do
  it "has none to begin with" do
    expect(Task.count).to eq 0
  end
  it "has 0 after adding with blank params" do
    Task.create
    expect(Task.count).to eq 0
  end
  #it "has 1 after adding with email" do
  #  Task.create(name: "#{Faker::Space.planet}",start: Time.now, user_id: 1)
  #  expect(Task.count).to eq 1
  #end

  #it "has to be valid" do
  #  @Task1 = create(:Task)
  #  expect(@Task1).to be_valid
  #end


end
