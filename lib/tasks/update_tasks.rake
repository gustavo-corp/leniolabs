directory "tasks"

task :out_of_date => :environment do
  Task.out_of_date
end

task :before_expired => :environment do
  Task.before_expired
end