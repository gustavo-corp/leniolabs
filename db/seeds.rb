def rr(name)# random record
  if name == "user"
    User.where( id:(1+rand(User.count) ) ).last
  elsif name == "task"
    Task.where( id:(1+rand(Task.count) ) ).last
  elsif name == "project"
    Project.where( id:(1+rand(Project.count) ) ).last
  end
end

normal_user = User.create(email: "normal@user.com", password: "123123", name: Faker::Name.name )

first_user = User.create(email: "g@g.g", password: "123123", name: Faker::Name.name )
first_user.add_role(:admin)

second_user = User.create(email: "admin@admin.com", password: "123123", name: Faker::Name.name )
second_user.add_role(:admin)

i = 0
10.times do 
  project = Project.create(name: "#{Faker::Space.planet + i.to_s}",start: Time.now - rand(5000).days ,end: Time.now + rand(5000).days, user: first_user, kind: rand(4) )
  Comment.create(user: first_user, commentable_id: project.id,body: Faker::Quotes::Shakespeare.hamlet_quote, commentable_type: Project)
  10.times do
    task = Task.create(name: Faker::BossaNova.artist, project_id: project.id, priority: (rand(100) + 1), end_date: Time.now + rand(500).days, deadline_days: rand(4))
    Comment.create(user: first_user, commentable_id: rr("task").id,body: Faker::Quotes::Shakespeare.as_you_like_it_quote, commentable_type: Task)
  end
  i +=1
end

10.times do 
  User.create(email: Faker::Internet.email, password: "123123", name: Faker::Name.name )
end

25.times do 
  project = Project.create(name: Faker::Space.planet,start: Time.now - rand(5000).days, end: Time.now + rand(5000).days, user: rr("user"), kind: rand(4) )
  10.times do
    task = Task.create(name: Faker::BossaNova.artist, project_id: project.id, priority: (rand(100) + 1),deadline_days: rand(4))
  end
end


10.times do
  task = Task.create(name: Faker::BossaNova.artist, priority: (rand(100) + 1))
end

250.times do 
  Comment.create(user: rr("user"), commentable_id: rr("project").id,body: Faker::Quotes::Shakespeare.hamlet_quote, commentable_type: Project)
  Comment.create(user: rr("user"), commentable_id: rr("task").id,body: Faker::Quotes::Shakespeare.as_you_like_it_quote, commentable_type: Task)
end
